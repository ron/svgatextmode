/*
 * Mostly a dummy to get things compiling
 * without having to have X11 around.
 *
 * courtesy of David Mosberger-Tang <davidm@cs.arizona.edu>
 */
#define _XFUNCPROTOBEGIN
#define _XFUNCPROTOEND

#ifdef __STDC__
# define NeedFunctionPrototypes        1
#else
# define NeedFunctionPrototypes        0
#endif
